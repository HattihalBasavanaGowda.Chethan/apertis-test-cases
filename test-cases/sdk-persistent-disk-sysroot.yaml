metadata:
  name: sdk-persistent-disk-sysroot
  format: "Apertis Test Definition 1.0"
  image-types:
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
  type: compatibility
  exec-type: manual
  priority: critical
  maintainer: "Apertis Project"
  description: "Tests that persistent disk can be used when upgrading the SDK."

  pre-conditions:
    - "Download the virtual machine image for the latest SDK release from:"
    - "~https://images.apertis.org/"
    - "Make a note of release version (e.g v2020dev0)"
    - "Instructions on how to use the SDK persistent workspace on VirtualBox:"
    - "~https://developer.apertis.org/latest/sdk-usage.html#persistent-workspace"
    - "Clone the apertis-tests repository:"
    - $ git clone https://gitlab.apertis.org/infrastructure/apertis-tests.git

  expected:
    - "1) When attempting to confirm if the SDK is using the persistent disk psdk
       should indicate that 'This SDK is currently using the persistent disk'."
    - "2) 'ade sysroot update' should indicate that the installed version is
       already up-to-date."

  notes:
    - The psdk-test script will ask for the Apertis image SSH password in order to
      access the virtual machine during some stages.
    - Sometimes the SSH connection might show the error "Connection reset by peer",
      in such a case, please start again the test.
    - The psdk-test script starts and poweroff the virtual machine several times, if
      something goes wrong during booting or turning off a VM, please manually
      close and restart the virtual machine and continue executing the test.

run:
  steps:
    - "Execute the psdk-test script from the apertis-tests repository, passing the
       image release, the path to the Apertis SDK VDI image and with test type
       'sysroot', for example:"
    - $ cd apertis-tests/psdk-test
    - $ ./psdk-test sysroot <image_release> <apertis_sdk_image>.vdi
    - "The psdk-test command will open an interactive session that can be used
       to execute the test through different stages. The prompt should look like:"
    - $ psdk-test>
    - "Execute the first stage of the test entering 'r':"
    - $ psdk-test> r
    - The stage1 command will setup the test environment (create and clone VMs)
      and also will execute any necessary step for the initialization of the test.
      After execution of stage1, there should be a virtual machine running
      named apertis-psdk-test-old-sdk, proceed to the next step.
    - "Use psdk to initialize the empty disk and to configure the SDK to use it:"
    - ~https://developer.apertis.org/latest/sdk-usage.html#preparing-the-persistent-disk-on-the-old-sdk
    - "The SDK will reboot when the configuration is completed. After the reboot
       click on psdk icon again to confirm that the persistent disk is in use."
    - "Continue executing the second stage of the test:"
    - $ psdk-test> r
    - "After the execution of stage2, there should be a virtual machine running
       named apertis-psdk-test-new-sdk, proceed to the next step."
    - "Use psdk to configure the SDK to use the persistent disk:"
    - ~https://developer.apertis.org/latest/sdk-usage.html#using-the-persistent-disk-on-the-new-sdk
    - The SDK will reboot when the configuration is completed. After the reboot
      click on psdk icon again to confirm that the persistent disk is in use.
    - "Execute the final stage:"
    - $ psdk-test> r
    - This will complete the execution of the test, please check that the expected
      result is obtained.
